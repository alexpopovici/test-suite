package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FullPostPage {
    private WebDriver driver;

    @FindBy(xpath = "//time[@class='entry-date published updated']")
    private WebElement postPublishedDate;

    @FindBy(xpath = "//h1[@class='entry-title']")
    private WebElement postTitle;

    @FindBy(xpath = "//div[@class='entry-content clearfix']")
    private WebElement postBodyText;

    @FindBy(xpath = "//div[@class='entry-categories clearfix']")
    private WebElement postCategories;

    @FindBy(xpath = "//div[@class='entry-tags clearfix']")
    private WebElement postTags;

    @FindBy(xpath = "//div[@class='nav-previous']")
    private WebElement previousPost;

    @FindBy(xpath = "nav-next")
    private WebElement nextPost;

    @FindBy(xpath = "//h3[@id='reply-title']")
    private WebElement leaveAReplyTitle;

    @FindBy(xpath = "//p[@class='comment-notes']")
    private WebElement replyNotificationMessage;

    @FindBy(xpath = "//label[@for='comment']")
    private WebElement commentBoxLabel;

    @FindBy(xpath = "//textarea[@id='comment']")
    private WebElement commentBoxTextArea;

    @FindBy(xpath = "//label[@for='author']")
    private WebElement commentNameLabel;

    @FindBy(xpath = "//input[@id='author']")
    private WebElement commentNameField;

    @FindBy(xpath = "//label[@for='email']")
    private WebElement commentEmailLabel;

    @FindBy(xpath = "//input[@id='email']")
    private WebElement commentEmailField;

    @FindBy(xpath = "//label[@for='url']")
    private WebElement commentWebsiteLabel;

    @FindBy(xpath = "//input[@id='url']")
    private WebElement commentWebsiteField;

    @FindBy(xpath = "//input[@id='submit']")
    private WebElement commentSubmitBTN;

    public FullPostPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


}
