package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class SearchResultsPage {
    private WebDriver driver;

    @FindBy(xpath = "//div[@id='post-wrapper']/div[@class='post-column clearfix']")
    private WebElement searchResultItem;

    @FindBy(xpath = "//h1[@class='archive-title']")
    private WebElement searchResultsTitle;

    public WebElement getSearchResultsTitle() {
        return searchResultsTitle;
    }

    public List<WebElement> searchResultAllItems;

    public List<WebElement> getSearchResultAllItems() {
        searchResultAllItems = driver.findElements(By.xpath("//div[@id='post-wrapper']/div[@class='post-column clearfix']"));
        return searchResultAllItems;
    }


    public SearchResultsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
