package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ShopCategoryPage {
    private WebDriver driver;

    public List<WebElement> allProductsOnOneCategoryPage;

    public List<WebElement> getAllProductsOnOneCategoryPage() {
        allProductsOnOneCategoryPage = driver.findElements(By.xpath("//h3[@class='products-title']"));
        return allProductsOnOneCategoryPage;
    }

    @FindBy(xpath = "//h3[@class='products-title']")
    private WebElement singleProductTitle;

    public WebElement getSingleProduct() {
        return singleProduct;
    }

    public WebElement getProductOldPrice() {
        return productOldPrice;
    }

    @FindBy(xpath = "//li[@class[contains(.,'product')]]")
    private WebElement singleProduct;

    @FindBy(xpath = "//div[@class='products-hover-block']//i[@class='fa fa-search-plus']")
    private WebElement productPreviewLinkBTN;

    public WebElement getProductPreviewImageFull() {
        return productPreviewImageFull;
    }

    @FindBy(xpath = "//div[@class[contains(.,'pp_content')]]//img[@id='fullResImage']")
    private WebElement productPreviewImageFull;

    public List<WebElement> productsOnSale;

    public List<WebElement> getProductsOnSale() {
        productsOnSale = driver.findElements(By.xpath("//li[@class[contains(.,'sale')]]"));
        return productsOnSale;
    }

    @FindBy(xpath = "//li[@class[contains(.,'product')]]//del")
    private WebElement productOldPrice;


    public void clickPreviewForSingleProduct(Integer productNumber) {

        WebElement singleProduct = driver.findElement(By.xpath("//li[@class[contains(., 'product ')]][" + productNumber + "]"));
        WebElement singleProductPreviewBTN = driver.findElement(By.xpath("//li[@class[contains(., 'product ')]][" + productNumber + "]//div[@class='products-hover-block']/a/i[@class='fa fa-search-plus']"));

        Actions builder = new Actions(driver);
        builder.moveToElement(singleProduct).build().perform();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(singleProductPreviewBTN));

        singleProductPreviewBTN.click();
        wait.until(ExpectedConditions.visibilityOf(productPreviewImageFull));
    }


    public ShopCategoryPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
