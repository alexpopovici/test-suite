package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ShopHomePage {
    private WebDriver driver;

    @FindBy(xpath = "//div[@class='search-wrapper search-user-block']/div[@class='search-icon']")
    private WebElement searchIcon;

    @FindBy(xpath = "//div[@class='header-search-box active']//input[@type='search']")
    private WebElement searchField;

    @FindBy(xpath = "//div[@class='header-search-box active']//button[@type='submit']")
    private WebElement searchBTN;

    public ShopHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public ShopSearchResultsPage searchFor(String searchTerm) {
        searchIcon.click();
        searchField.sendKeys(searchTerm);
        searchBTN.click();

        return new ShopSearchResultsPage(driver);

    }
}
