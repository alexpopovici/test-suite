package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ShopCartPage {
    private WebDriver driver;

    @FindBy(xpath = "//tr[@class='woocommerce-cart-form__cart-item cart_item']")
    private WebElement cartProduct;

    @FindBy(xpath = "//input[@class='input-text qty text']")
    private WebElement quantityField;

    public WebElement getQuantityField() {
        return quantityField;
    }

    @FindBy(xpath = "//input[@name='update_cart']")
    private WebElement updateCartBTN;

    @FindBy(xpath = "//div[@class='woocommerce-message'][contains(.,\"Cart updated\")]")
    private WebElement cartSuccsessfullyUpdatedMSG;


    public ShopCartPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public ShopCartPage updateFirstProductQuantity(String newQuantity) {
        quantityField.clear();
        quantityField.sendKeys(newQuantity);
        updateCartBTN.click();
        return new ShopCartPage(driver);
    }

    private WebElement cartItemThis;

    public ShopCartPage updateNthCartProductQuantity(int nThItem, String newQuantity) {

        cartItemThis = driver.findElement(getNthItemLocator(nThItem));
        cartItemThis.clear();
        cartItemThis.sendKeys(newQuantity);
        updateCartBTN.click();

        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.visibilityOf(cartSuccsessfullyUpdatedMSG));

        return new ShopCartPage(driver);
    }

    public By getNthItemLocator(int nThItem) {
        return By.xpath("//tr[@class='woocommerce-cart-form__cart-item cart_item'][" + nThItem + "]//input[@class='input-text qty text']");
    }

    public WebElement getNthItemElement(int nTh) {
        return driver.findElement(getNthItemLocator(nTh));
    }

}
