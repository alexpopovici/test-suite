package pages;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Base64;

public class ShopProductPage {
    private WebDriver driver;

    @FindBy(xpath = "//li[@id='tab-title-reviews']")
    private WebElement reviewsTab;

    @FindBy(xpath = "//a[@class='star-3']")
    private WebElement reviewStar3;

    @FindBy(xpath = "//textarea[@id='comment']")
    private WebElement reviewText;

    @FindBy(xpath = "//input[@id='author']")
    private WebElement reviewName;

    @FindBy(xpath = "//input[@id='email']")
    private WebElement reviewEmail;

    @FindBy(xpath = "//input[@id='submit']")
    private WebElement reviewSubmitBTN;

    @FindBy(xpath = "//input[@class='input-text qty text']")
    private WebElement productQuantityField;

    @FindBy(xpath = "//button[@name='add-to-cart']")
    private WebElement productAddToCartBTN;

    @FindBy(xpath = "//em[@class='woocommerce-review__awaiting-approval']")
    private WebElement reviewWaitingApprovalMessage;

    public WebElement getReviewWaitingApprovalMessage() {
        return reviewWaitingApprovalMessage;
    }

    @FindBy(xpath = "//h1[@class='product_title entry-title']")
    private WebElement productTitle;

    public WebElement getProductTitle() {
        return productTitle;
    }

    @FindBy(xpath = "//div[@class='woocommerce-message']")
    private WebElement productSuccessfullyAddedToCartMSG;

    public WebElement getProductSuccessfullyAddedToCartMSG() {
        return productSuccessfullyAddedToCartMSG;
    }

    @FindBy(xpath = "//div[@class='cart-wrapper']//div/a/i[@class='fa fa-shopping-cart']")
    private WebElement cartIcon;


    public ShopProductPage addProductToCart(String quantity){
        productQuantityField.clear();
        productQuantityField.sendKeys(quantity);
        productAddToCartBTN.click();

        return new ShopProductPage(driver);
    }

    public ShopProductPage sendReview() {
        reviewsTab.click();
        reviewStar3.click();
        reviewText.sendKeys(RandomStringUtils.randomAlphabetic(50));
        reviewName.sendKeys(RandomStringUtils.randomAlphabetic(12));
        reviewEmail.sendKeys("client" + RandomStringUtils.randomAlphabetic(10) + "@gmail.com");
        reviewSubmitBTN.click();

        return new ShopProductPage(driver);
    }

    public ShopCartPage goToCart(){
        cartIcon.click();
        return new ShopCartPage(driver);
    }


    public ShopProductPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
