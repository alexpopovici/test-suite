package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ContactPage {
    private WebDriver driver;

    @FindBy(xpath = "//input[@name='your-name']")
    private WebElement nameField;

    @FindBy(xpath = "//input[@name='your-email']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@name='tel-437']")
    private WebElement phoneField;

    @FindBy(xpath = "//input[@name='your-subject']")
    private WebElement subjectField;

    @FindBy(xpath = "//textarea[@name='your-message']")
    private WebElement messageField;

    @FindBy(xpath = "//select[@name='Select' and @class='wpcf7-form-control wpcf7-select wpcf7-validates-as-required']")
    private WebElement topicSelector;

    @FindBy(xpath = "//span[@class='wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required']")
    private WebElement whereDidYouFindUsBoxes;

    @FindBy(xpath = "//span[@class='wpcf7-form-control wpcf7-radio']")
    private WebElement professionRadio;

    @FindBy(xpath = "//input[@type='submit' and @value='Send']")
    private WebElement submitBTN;

    public WebElement getSuccessMSG() {
        return successMSG;
    }

    @FindBy(xpath = "//div[@class='wpcf7-response-output wpcf7-display-none wpcf7-mail-sent-ok']")
    private WebElement successMSG;

    public WebElement getFailMSG() {
        return failMSG;
    }

    @FindBy(xpath = "//div[@class='wpcf7-response-output wpcf7-display-none wpcf7-validation-errors']")
    private WebElement failMSG;


    public void yes(){
        successMSG.getText();
    }

    public void selectTopic(String topic) {
        List<WebElement> allTopics = topicSelector.findElements(By.xpath("//select[@name='Select']/option"));

        for (WebElement option : allTopics) {
            if (option.getText().equals(topic)) {
                option.click();
                break;
            }
        }
    }

    public void selectFindUs(String findUs) {
        List<WebElement> findUsBoxes = whereDidYouFindUsBoxes.findElements(By.xpath("//span[@class='wpcf7-form-control-wrap Wheredidyoufindus']//label"));

        for (WebElement box : findUsBoxes) {
            if (box.getText().equals(findUs)) {
                box.click();
                break;
            }

        }
    }

    public void selectProfession(String professionName) {
        List<WebElement> professionRadioOption = professionRadio.findElements(By.xpath("//span[@class='wpcf7-form-control-wrap profession']//label"));

        for (WebElement profession : professionRadioOption) {
            if (profession.getText().equals(professionName)) {
                profession.click();
                break;
            }
        }
    }

    public void submitForm(){
        submitBTN.click();
    }

    public ContactPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void fillFormWith(String myName, String myEmail, String myPhone, String mySubject, String myMessage) {
        nameField.sendKeys(myName);
        emailField.sendKeys(myEmail);
        phoneField.sendKeys(myPhone);
        subjectField.sendKeys(mySubject);
        messageField.sendKeys(myMessage);
    }

    public void fillRequiredFields(String myName, String myEmail, String myPhone){
        nameField.sendKeys(myEmail);
        emailField.sendKeys(myEmail);
        phoneField.sendKeys(myPhone);
    }
}
