package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class BlogHomePage {
    private WebDriver driver;

    @FindBy(xpath = "//ul[@id='menu-navigation']/li/a[contains(.,'Login')]")
    private WebElement loginLink;

    @FindBy(xpath = "//ul[@id='menu-navigation']/li/a[contains(.,'Contact')]")
    private WebElement contactLink;

    @FindBy(xpath = "//aside[@id='search-3']//input[@class='search-field']")
    private WebElement searchField;

    @FindBy(xpath = "//aside[@id='search-3']//button[@class='search-submit']")
    private WebElement searchBTN;

    @FindBy(xpath = "//aside[@id='archives-2']")
    private WebElement archivesMenu;

    @FindBy(xpath = "//aside[@id='categories-2']")
    private WebElement categoriesMenu;

    @FindBy(xpath = "//div[@id='tzwb-tabbed-content-2-tab-0']/ul[@class='tzwb-tabcontent-popular-posts tzwb-posts-list']")
    private WebElement recommendedPopular;

    @FindBy(xpath = "//div[@id='tzwb-tabbed-content-2-tab-1']/ul[@class='tzwb-tabcontent-comments tzwb-comments-list']")
    private WebElement recommendedComments;

    @FindBy(xpath = "//div[@id='tzwb-tabbed-content-2-tab-2']/ul[@class='tzwb-tabcontent-recent-posts tzwb-posts-list']")
    private WebElement recommendedRecent;

    public List<WebElement> recommendedPopularPostsList;

    public List<WebElement> getRecommendedPopularPostsList() {
        recommendedPopularPostsList = driver.findElements(By.xpath("//div[@id='tzwb-tabbed-content-2-tab-0']/ul/li"));
        return recommendedPopularPostsList;
    }

    public List<WebElement> recommendedCommentsList;

    public List<WebElement> getRecommendedCommentsList() {
        recommendedCommentsList = driver.findElements(By.xpath("//div[@id='tzwb-tabbed-content-2-tab-1']/ul/li"));
        return recommendedCommentsList;
    }

    public List<WebElement> recommendedRecentList;

    public List<WebElement> getRecommendedRecentList() {
        recommendedRecentList = driver.findElements(By.xpath("//div[@id='tzwb-tabbed-content-2-tab-2']/ul/li"));
        return recommendedRecentList;
    }



    public BlogHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void chooseArchiveMonth(String archiveMonth) {
        List<WebElement> archiveMonthsList = archivesMenu.findElements(By.xpath("//aside[@id='archives-2']//li"));

        for (WebElement month : archiveMonthsList) {
            if(month.getText().equalsIgnoreCase(archiveMonth)) {
                month.click();
                break;
            }
        }
    }

    public void chooseCategory(String category) {
        List<WebElement> categoriesList = categoriesMenu.findElements(By.xpath("//aside[@id='categories-2']//li"));

        for (WebElement categoryItem: categoriesList) {
            if(categoryItem.getText().equalsIgnoreCase(category)) {
                categoryItem.click();
                break;
            }

        }
    }

    public ContactPage navigateToContactPage(){
        contactLink.click();
        return new ContactPage(driver);
    }

    public LogInPage navaigateToLogInPage() {
        loginLink.click();
        return new LogInPage();
    }

    public SearchResultsPage searchForThis(String searchTerm) {
        searchField.sendKeys(searchTerm);
        searchBTN.click();
        return new SearchResultsPage(driver);
    }

    // --> In the Recommended Box click one of the tabs and click a post by number

//    public clickRecommendedPopularPost(Integer popularPostNumber) {
//        searchBTN.click();
//        return new FullPostPage(driver);
//    }

}
