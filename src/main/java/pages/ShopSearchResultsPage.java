package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ShopSearchResultsPage {
    private WebDriver driver;

    public List<WebElement> searchResultItems;

    public List<WebElement> getSearchResultItems() {
        searchResultItems = driver.findElements(By.xpath("//article/h2[@class='entry-title']"));
        return searchResultItems;
    }

    public ShopSearchResultsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

}
