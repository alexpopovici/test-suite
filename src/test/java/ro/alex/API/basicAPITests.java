package ro.alex.API;

import io.restassured.http.ContentType;
import io.restassured.http.Headers;
import io.restassured.*;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class basicAPITests {

    @Test
    public void petTest() {
        int s = 8;
        String createPetRequest = "{\n" +
                "    \"id\": " + s + ",\n" +
                "    \"category\": {\n" +
                "        \"id\": 2,\n" +
                "        \"name\": \"paganel\"\n" +
                "    },\n" +
                "    \"name\": \"doggie\",\n" +
                "    \"photoUrls\": [\n" +
                "        \"string\"\n" +
                "    ],\n" +
                "    \"tags\": [\n" +
                "        {\n" +
                "            \"id\": 3,\n" +
                "            \"name\": \"wantsomePetTag1\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"status\": \"available\"\n" +
                "}";
        given().contentType(ContentType.JSON).body(createPetRequest)
                .when().put("https://petstore.swagger.io/v2/pet");

        given().contentType("application/json")
                .when().get("https://petstore.swagger.io/v2/pet/" + s)
                .then().body("id", is(8))
                .body("category.name", is("paganel"))
                .body("status", is("available"))
                .statusCode(200);
    }

    @Test
    public void jsonParsingTest() {
        get("https://petstore.swagger.io/v2/pet/3")
                .then().body("tags.findAll {it.id < 100}[0].name",
                equalTo("wantsomePetTag1"));
    }



    @Test
    public void getResponseData() {
        Response response = given().contentType("application/json").
                when().get("http://petstore.swagger.io/v2/pet/665");

        response.prettyPrint();

        int id = response.path("id");
        assertThat(id, is(665));

        String jsonResponse = response.asString();
        JsonPath jsonPath = new JsonPath(jsonResponse);
        String categoryName = jsonPath.getString("category.name");
        assertThat(categoryName, is("XsatanicaX"));
    }



    @Test
    public void additionalData() {
        Response response = given().contentType("application/json").
                when().get("http://petstore.swagger.io/v2/pet/12345");

        // Get all headers
        Headers allHeaders = response.getHeaders();

        // Get a single header value:
        String headerValue = response.getHeader("Content-Type");
        assertThat(headerValue, is("application/json"));

        // Get status code
        int statusCode = response.getStatusCode();
        assertThat(response.getStatusCode(), is(200));
    }




}
