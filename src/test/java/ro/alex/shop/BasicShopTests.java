package ro.alex.shop;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.*;
import ro.alex.blog.BaseTestClass;

public class BasicShopTests extends BaseTestClass {
    public String search50Chars = RandomStringUtils.randomAlphabetic(50);

    @Test
    public void searchSupports50CharactersTest() {

        driver.get("http://practica.wantsome.ro/shop/");

        ShopHomePage shp = new ShopHomePage(driver);
        shp.searchFor(search50Chars);

        ShopSearchResultsPage ssrp = new ShopSearchResultsPage(driver);

        WebElement searchResultMessage = driver.findElement(By.xpath("//div[@id='crumbs']/span[2]"));

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOf(searchResultMessage));

        Assert.assertEquals("Search Results for \"" + search50Chars + "\" query", searchResultMessage.getText());
    }

    @Test
    public void categoryContainsMoreThanZeroProductsTest() {
        driver.get("http://practica.wantsome.ro/shop/?product_cat=men-collection");

        ShopCategoryPage scp = new ShopCategoryPage(driver);

        Assert.assertEquals(10, scp.getAllProductsOnOneCategoryPage().size());
    }

    @Test
    public void simpleSearchTest() {

        driver.get("http://practica.wantsome.ro/shop/");

        ShopHomePage shp = new ShopHomePage(driver);
        shp.searchFor("jeans");

        ShopSearchResultsPage ssrp = new ShopSearchResultsPage(driver);

        WebElement searchResultMessage = driver.findElement(By.xpath("//div[@id='crumbs']/span[2]"));

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOf(searchResultMessage));

        Assert.assertTrue(ssrp.getSearchResultItems().size() >= 2);
    }

    @Test
    public void addProductReviewTest() {
        driver.get("http://practica.wantsome.ro/shop/?product=mens-watch");

        ShopProductPage spp = new ShopProductPage(driver);

        spp.sendReview();

        Assert.assertTrue(spp.getReviewWaitingApprovalMessage().isDisplayed());
    }

    @Test
    public void addProductToCart() {
        driver.get("http://practica.wantsome.ro/shop/?product=mens-watch");
        ShopProductPage spp = new ShopProductPage(driver);

        spp.addProductToCart("1");

        Assert.assertTrue(spp.getProductSuccessfullyAddedToCartMSG().isDisplayed());
    }

    @Test
    public void addProductToCartAndUpdateQuantity() {

        String newQuantity = "3";
        driver.get("http://practica.wantsome.ro/shop/?product=mens-watch");
        ShopProductPage spp = new ShopProductPage(driver);

        spp.addProductToCart("1");

        spp.goToCart();

        ShopCartPage scp = new ShopCartPage(driver);

        scp.updateFirstProductQuantity(newQuantity);

        Assert.assertEquals(newQuantity, scp.getQuantityField().getAttribute("value"));
    }

    @Test
    public void addProductsToCartAndUpdateNthItemQuantity() {

        String newQuantity = "8";
        int nThItemToUpdate = 2;

        driver.get("http://practica.wantsome.ro/shop/?product=mens-watch");
        ShopProductPage spp = new ShopProductPage(driver);
        spp.addProductToCart("1");

        driver.get("http://practica.wantsome.ro/shop/?product=mens-silk-ties");
        spp.addProductToCart("2");
        spp.goToCart();
        ShopCartPage scp = new ShopCartPage(driver);
        scp.updateNthCartProductQuantity(nThItemToUpdate, newQuantity);

        Assert.assertEquals(newQuantity, scp.getNthItemElement(nThItemToUpdate).getAttribute("value"));
    }


}
