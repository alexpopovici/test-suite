package ro.alex.shop;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.ShopCategoryPage;
import ro.alex.blog.BaseTestClass;

public class CategoryTests extends BaseTestClass {

    @Test
    public void openProductPreviewIMGTest() {

        int nThProduct = 6;

        driver.get("http://practica.wantsome.ro/shop/?product_cat=women-collection");
        ShopCategoryPage scp = new ShopCategoryPage(driver);
        scp.clickPreviewForSingleProduct(nThProduct);
        String imageWidthAsString = scp.getProductPreviewImageFull().getAttribute("width");
        int realImageWidth = Integer.parseInt(imageWidthAsString);

        Assert.assertTrue(realImageWidth > 10);
    }

    @Test
    public void productsWithOnSaleTagHaveDiscountedPrice(){
        driver.get("http://practica.wantsome.ro/shop/?product_cat=women-collection");
        ShopCategoryPage scp = new ShopCategoryPage(driver);

        for (WebElement saleProduct : scp.getProductsOnSale()) {
            Assert.assertTrue(saleProduct.findElement(By.xpath(".//del/span")).isDisplayed());
            Assert.assertTrue(saleProduct.findElement(By.xpath(".//div[@class='sales-tag']")).isDisplayed());
        }
    }



}
