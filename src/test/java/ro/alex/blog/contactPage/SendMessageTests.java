package ro.alex.blog.contactPage;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.BlogHomePage;
import pages.ContactPage;
import ro.alex.blog.BaseTestClass;

import java.util.concurrent.TimeUnit;

public class SendMessageTests extends BaseTestClass {


    private String validName = "Alexandru Ioan Cuza";
    private String invalidName = "";
    private String validEmail = "cuza@moldova.ro";
    private String invalidEmail = "cuza@moo";
    private String validPhone = "1234567890";
    private String invalidPhone = "+123456789";
    private String validSubject = "Hello, my friend";
    private String longSubject = RandomStringUtils.randomAlphabetic(5000);
    private String validMessage = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer porttitor ligula vitae imperdiet lobortis. Cras pharetra velit ut nunc sodales consectetur.";
    private String longMessage = RandomStringUtils.randomAlphanumeric(10000);
    private String topic = "Technical details";
    private String whereDidYouFindUs = "Friends";
    private String profession = "Sales";
    private String successMessage = "Thank you for your message. It has been sent.";
    private String failMessage = "One or more fields have an error. Please check and try again.";


    @Before
    public void setupImplicitWait() {
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
    }

    @Test
    public void sendMessageWithValidDataTest() {

        BlogHomePage bhp = new BlogHomePage(driver);
        bhp.navigateToContactPage();

        ContactPage contactPage = new ContactPage(driver);
        contactPage.fillFormWith(validName, validEmail, validPhone, validSubject, validMessage);
        contactPage.selectTopic(topic);
        contactPage.selectFindUs(whereDidYouFindUs);
        contactPage.selectProfession(profession);
        contactPage.submitForm();

        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='wpcf7-response-output wpcf7-display-none wpcf7-mail-sent-ok']")));

        Assert.assertTrue(contactPage.getSuccessMSG().isDisplayed());
    }

    @Test
    public void submitFormWithLongMessageTest() {

        BlogHomePage bhp = new BlogHomePage(driver);
        bhp.navigateToContactPage();

        ContactPage contactPage = new ContactPage(driver);
        contactPage.fillFormWith(validName, validEmail, validPhone, validSubject, longMessage);
        contactPage.selectTopic(topic);
        contactPage.selectFindUs(whereDidYouFindUs);
        contactPage.selectProfession(profession);
        contactPage.submitForm();

        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='wpcf7-response-output wpcf7-display-none wpcf7-mail-sent-ok']")));

        Assert.assertTrue(contactPage.getSuccessMSG().isDisplayed());
    }

    @Test
    public void submitFormWithRequiredFieldsOnlyTest() {

        BlogHomePage bhp = new BlogHomePage(driver);
        bhp.navigateToContactPage();

        ContactPage contactPage = new ContactPage(driver);
        contactPage.fillRequiredFields(validName, validEmail, validPhone);
        contactPage.submitForm();

        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='wpcf7-response-output wpcf7-display-none wpcf7-validation-errors']")));

        Assert.assertTrue(contactPage.getFailMSG().isDisplayed());
    }
}
