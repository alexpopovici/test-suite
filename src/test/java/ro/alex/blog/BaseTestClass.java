package ro.alex.blog;

import org.apache.commons.lang3.SystemUtils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTestClass {
    public WebDriver driver;
//    String url = "http://practica.wantsome.ro/shop/";
    private DriversPath driversPath = new DriversPath();

    @Before
    public void setUpTest() {
        System.setProperty("webdriver.chrome.driver", driversPath.getDriverDirPath() + "chromedriver" + driversPath.getDriverExtension());
        driver = new ChromeDriver();
        if (SystemUtils.IS_OS_WINDOWS) {
            driver.manage().window().maximize();
        }
//        driver.get(url);

    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
