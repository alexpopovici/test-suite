package ro.alex.blog.homePage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pages.BlogHomePage;
import ro.alex.blog.BaseTestClass;

import java.util.concurrent.TimeUnit;

public class RecommendedBoxTests extends BaseTestClass {

    @Before
    public void setupImplicitWait() {
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
    }

    @Test
    public void popularPostsNumberTest() {
        BlogHomePage bhp = new BlogHomePage(driver);

        Assert.assertEquals(5, bhp.getRecommendedPopularPostsList().size());
        Assert.assertEquals(5, bhp.getRecommendedCommentsList().size());
        Assert.assertEquals(5, bhp.getRecommendedRecentList().size());
    }
}
