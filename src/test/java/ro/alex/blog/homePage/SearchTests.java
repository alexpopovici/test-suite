package ro.alex.blog.homePage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.BlogHomePage;
import pages.SearchResultsPage;
import ro.alex.blog.BaseTestClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class SearchTests extends BaseTestClass {


    private String searchTerm1 = "Automated";
    private String searchTerm2 = "Wantsome";
    private String searchTerm3 = "1509572607748";


    @Before
    public void setupImplicitWait() {
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
    }

    @Test
    public void simpleSearchTest() {
        BlogHomePage bhp = new BlogHomePage(driver);
        bhp.searchForThis(searchTerm2);

        SearchResultsPage srp = new SearchResultsPage(driver);

        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='archive-title']")));

//        Assert.assertTrue(SearchResultsPage.getSearchResultsTitle().isDisplayed());
        Assert.assertTrue(srp.getSearchResultsTitle().isDisplayed());
    }

    @Test
    public void simpleSearchWithOneResultTest() {
        BlogHomePage bhp = new BlogHomePage(driver);
        bhp.searchForThis(searchTerm3);

        SearchResultsPage srp = new SearchResultsPage(driver);

        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='archive-title']")));

        Assert.assertEquals(1, srp.searchResultAllItems.size());
    }

}
